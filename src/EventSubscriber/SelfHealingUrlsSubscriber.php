<?php

declare(strict_types = 1);

namespace Drupal\self_healing_urls\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * URL healing for entities found with the provided ID.
 */
final class SelfHealingUrlsSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a SelfHealingUrlsSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::EXCEPTION => ['onKernelException'],
    ];
  }

  /**
   * Hook into the NotFoundHttpException and attempt to fix the URL if feasible.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The Event to process.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function onKernelException(ExceptionEvent $event): void {
    $request = $event->getRequest();
    $path = $request->getRequestUri();
    if ($event->getThrowable() instanceof NotFoundHttpException) {
      // For now an assumption is made
      // that the URL will always contain the node ID,
      // in the first location of the URL path.
      $nodeId = explode('/', $path)[1];
      $nodeStorage = $this->entityTypeManager->getStorage('node');
      $node = $nodeStorage->load($nodeId);
      if ($node) {
        $url = $node->toUrl()->toString();
        $redirectResponse = new RedirectResponse($url, 301);
        $event->setResponse($redirectResponse);
      }
    }
  }

}
